# Ansible Role: Node Exporter

An Ansible role that deploys the Prometheus Node Exporter for Raspberry Pi and other architectures.

 
Source :point_right: https://linuxhit.com/prometheus-node-exporter-on-raspberry-pi-how-to-install/ 

 

## Requirements

None.



## Role Variables


Available variables listed below, along with default values (see `defaults/main.yml`)
```
# Version
vers: 1.8.2 # change accordingly and see https://github.com/prometheus/node_exporter/releases

# Prometheus URL
amd64_url: https://github.com/prometheus/node_exporter/releases/download/v{{ vers }}/node_exporter-{{ vers }}.linux-amd64.tar.gz
arm64_url: https://github.com/prometheus/node_exporter/releases/download/v{{ vers }}/node_exporter-{{ vers }}.linux-arm64.tar.gz
armv6_url: https://github.com/prometheus/node_exporter/releases/download/v{{ vers }}/node_exporter-{{ vers }}.linux-armv6.tar.gz
armv7_url: https://github.com/prometheus/node_exporter/releases/download/v{{ vers }}/node_exporter-{{ vers }}.linux-armv7.tar.gz

# Architecture
amd64_arch: amd64
arm64_arch: arm64
v6_arch:  armv6
v7_arch:  armv7

```

## Dependencies

 Prometheus: https://prometheus.io/docs/prometheus/latest/installation/
 
 Grafana: https://grafana.com/docs/grafana/latest/setup-grafana/installation/debian/




## Deploy

`$ ansible-playbook deploy.yml -K`

```yaml
---
- hosts: all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-node-exporter
```




## Next Steps

Add the host scrape metrics block on your prometheus server `/etc/prometheus/prometheus.yml`

```yaml
- job_name: 'raspberrypi'
    scrape_interval: 5s
    static_configs:
    - targets: ['192.168.1.x:9100']
```

Save and restart prometheus, checking `http://prometheus.server:9090/targets` to  see the metrics are being recieved.

If you have Grafana running theres an excellent node_exporter dashboard template [available](https://grafana.com/grafana/dashboards/1860-node-exporter-full/).




## Licence

MIT/BSD
